package main

import (
	"fmt"
	"gitlab.com/cjgajard/cars/tb-box" // box
	"gitlab.com/cjgajard/cars/world"
	"github.com/nsf/termbox-go" // termbox
)

const (
	TileNone world.TileType = iota
	TileDirt
	TileRoad
	TileRoadLane
	TileRoadLaneLeft
	TileRoadLaneRight
	TileWall
	TileWallLeft
	TileWallRight
)

type Printer interface {
	Init()
	Close()
	Draw(*Game)
}

type TermboxPrinter struct {
	game *Game
}

type MapTile struct {
	r rune
	fg termbox.Attribute
	bg termbox.Attribute
}

var TileRune = map[world.TileType]MapTile{
	TileNone: MapTile{
		r: ' ', fg: termbox.ColorDefault, bg: termbox.ColorBlack},
	TileDirt: MapTile{
		r: ' ', fg: termbox.ColorDefault, bg: ColorDirt},
	TileRoad: MapTile{
		r: ' ', fg: termbox.ColorDefault, bg: ColorAsphalt},
	TileRoadLane: MapTile{
		r: '\'', fg: termbox.ColorDefault, bg: ColorAsphalt},
	TileRoadLaneLeft: MapTile{
		r: '`', fg: termbox.ColorDefault, bg: ColorAsphalt},
	TileRoadLaneRight: MapTile{
		r: '\xb4', fg: termbox.ColorDefault, bg: ColorAsphalt},
	TileWall: MapTile{
		r: '|', fg: termbox.ColorDefault, bg: ColorAsphalt},
	TileWallLeft: MapTile{
		r: '\\', fg: termbox.ColorDefault, bg: ColorAsphalt},
	TileWallRight: MapTile{
		r: '/', fg: termbox.ColorDefault, bg: ColorAsphalt},
}

func (p TermboxPrinter) GetWallRune(x, y int, row world.Row) (MapTile, bool) {
	if wl, wr := row.Wall(); x == wl {
		if nrow, ok := p.game.world[y + 1]; ok {
			nwl, _ := nrow.Wall()
			if d := nwl - x; d > 0 {
				return TileRune[TileWallRight], true
			} else if d < 0 {
				return TileRune[TileWallLeft], true
			}
		}
		return TileRune[TileWall], true
	} else if x == wr {
		if nrow, ok := p.game.world[y + 1]; ok {
			_, nwr := nrow.Wall()
			if d := nwr - x; d > 0 {
				return TileRune[TileWallRight], true
			} else if d < 0 {
				return TileRune[TileWallLeft], true
			}
		}
		return TileRune[TileWall], true
	}
	return TileRune[TileNone], false
}

func (p TermboxPrinter) GetTileRune(x, y int) MapTile {
	row, ok := p.game.world[y]
	if !ok {
		return TileRune[TileNone]
	}

	if r, ok := p.GetWallRune(x, y, row); ok {
		return r
	}

	wl, wr := row.Wall()
	if x < wl || x > wr {
		return TileRune[TileDirt]
	}

	if x > wl && x < wr {
		if (x - row.Offset) % (world.CONFIG_LANE_WIDTH + 1) == 0 {
			if nrow, ok := p.game.world[y + 1]; ok {
				if nrow.Offset > row.Offset {
					return TileRune[TileRoadLaneRight]
				} else if nrow.Offset < row.Offset {
					return TileRune[TileRoadLaneLeft]
				}
			}
			return TileRune[TileRoadLane]
		}
		return TileRune[TileRoad]
	}

	col, ok := row.Tile[x]
	if !ok {
		return TileRune[TileNone]
	}

	return TileRune[col.Type]
}

func printLine(s string, y, offset int, fg, bg termbox.Attribute) {
	for x, c := range s {
		termbox.SetCell(x + offset, y, c, fg, bg)
	}
}

func relative(top, y int) int {
	return top - y - 1
}

func (p *TermboxPrinter) scoreBar() string {
	tmp := "score: %04.0f"
	return fmt.Sprintf(tmp, p.game.Score())
}

func (p *TermboxPrinter) statusBar() string {
	str := ""
	if !p.game.life {
		return ""
	}
	if p.game.player.car != nil {
		tmp := "%3d km/h"
		return fmt.Sprintf(tmp, p.game.player.speed)
	}
	return str
}

func (p *TermboxPrinter) DrawDialog() {
	if len(p.game.dialogQ) <= p.game.dialog {
		return
	}
	dialog := p.game.dialogQ[p.game.dialog]
	b := box.Print(ETC_DIALOG_WIDTH, dialog, box.AlignCenter)
	yo := ETC_DIALOG_TOP
	xo := (GAME_WIDTH - ETC_DIALOG_WIDTH) / 2
	c := termbox.ColorDefault
	for y, row := range b {
		for x, r := range row {
			termbox.SetCell(x + xo, y + yo, r, c, c)
		}
	}
}

func (p *TermboxPrinter) DrawUnit(u Unit) {
	cc := u.sprite.Draw()
	color := u.sprite.Color() + 1

	for _, c := range cc {
		x, y := c.XY()
		ux, uy := u.x + x, u.y + y
		mt := p.GetTileRune(ux, uy)
		fg := mt.fg
		if color >= 0 {
			fg = termbox.Attribute(color)
		}
		ry := relative(p.game.top, uy)
		termbox.SetCell(ux, ry, c.Rune(), fg, mt.bg)
	}
}

func (p *TermboxPrinter) Draw() {
	termbox.Clear(termbox.ColorDefault, termbox.ColorDefault)
	defer termbox.Flush()

	// Rows
	for y := 0; y < GAME_HEIGHT; y++ {
		gameY := relative(p.game.top, y)
		for x := 0; x < GAME_WIDTH; x++ {
			mt := p.GetTileRune(x, gameY)
			termbox.SetCell(x, y, mt.r, mt.fg, mt.bg)
		}
	}

	// Units
	for _, u := range p.game.unit {
		if u.y > p.game.Bottom() {
			p.DrawUnit(*u)
		}
	}
	p.DrawUnit(p.game.player.Unit)

	// UI
	c := termbox.ColorDefault
	printLine(p.statusBar(), GAME_HEIGHT, 0, c, c)

	str := p.scoreBar()
	printLine(str, GAME_HEIGHT, GAME_WIDTH - len(str), c, c)

	// Dialog boxes
	p.DrawDialog()
}

func (p *TermboxPrinter) Init() {
	termbox.Init()
	termbox.SetOutputMode(termbox.Output256)
}

func (p *TermboxPrinter) Close() {
	termbox.Close()
}
