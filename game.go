package main

import (
	"math/rand"
	"time"
	"gitlab.com/cjgajard/cars/world"
)

type Game struct {
	unit []*Unit
	top int
	player *Player
	world map[int]world.Row
	life bool
	win bool
	dialogQ [][2]string
	dialog int
}

func NewGame() *Game {
	p := NewPlayer()
	u := []*Unit{}
	w := map[int]world.Row{}

	return &Game{
		unit: u,
		top: GAME_HEIGHT,
		player: p,
		world: w,
		life: true,
		win: false,
		dialogQ: [][2]string{},
		dialog: 0,
	}
}

func (*Game) Size() (int, int) {
	return GAME_WIDTH, GAME_HEIGHT
}

func (g *Game) Top() int {
	return g.top
}

func (g *Game) Bottom() int {
	return g.top - GAME_HEIGHT
}

func (g *Game) World() map[int]world.Row {
	return g.world
}

func (g *Game) Lose() {
	if g.life {
		g.dialogQ = append(g.dialogQ, [2]string{
			"Takumi",
			"Oh fuck! You crashed the HACHI-ROKU!",
		})
		g.dialogQ = append(g.dialogQ, [2]string{
			"",
			"GAME OVER",
		})
	}
	g.life = false
}

func (g *Game) Win() {
	if !g.win {
		g.dialogQ = append(g.dialogQ, [2]string{
			"",
			"LEVEL CLEARED",
		})
	}
	g.win = true
}

func (g *Game) Init() {
	c := NewCar(GAME_WIDTH / 2, GAME_HEIGHT / 4, nil, nil)
	c.sprite.SetOption(STR_CAR_OPEN, STR_TRUE)
	g.unit = append(g.unit, c)

	c = NewCar(10, GAME_HEIGHT * 10, nil, nil)
	c.sprite.SetOption(STR_CAR_OPEN, STR_TRUE)
	g.unit = append(g.unit, c)

	g.dialogQ = append(g.dialogQ, [2]string{
		"Father",
		"Hurry up, Takumi! Deliver the T\xA0O\xA0F\xA0U!",
	})
	g.dialogQ = append(g.dialogQ, [2]string{
		"",
		"*Eurobeat intensifies*",
	})

	for y := 0; y < wgen.Height(); y++ {
		if rand.Float64() < RNG_CAR {
			if nrow, ok := g.World()[y]; ok {
				x := rand.Intn(nrow.Lane)
				x *= world.CONFIG_LANE_WIDTH + 1
				x += nrow.Offset + 2
				u := NewCar(x, y, nil, nil)
				g.unit = append(g.unit, u)
			}
		}
	}
}

func nextRowSize(exp, prev int) int {
	n := prev
	if exp > prev {
		n += 1
	} else if exp < prev {
		n -= 1
	}
	return n
}

func (g *Game) UpdateCamera(ch chan Event) {
	if !g.life || g.win {
		return
	}
	s := g.player.speed
	if s < 0 {
		s = -s
	}
	if s > 0 {
		d := time.Duration(
			CONFIG_PLAYER_SPEED_TIME *
			CONFIG_PLAYER_SPEED_LIMIT / s)
		time.Sleep(d * time.Millisecond)
		if g.player.speed > 0 {
			g.top++
			g.player.Move(UP, 1)
		} else {
			g.top--
			g.player.Move(DOWN, 1)
		}
	}
}

func (g *Game) Update() {
	if g.player.y >= wgen.Height() {
		g.Win()
		return
	}

	/*
	// Check player-wall collision
	row := g.world[g.player.y]
	if o := row.offset; g.player.x == o || g.player.x == o + row.size {
		g.Lose()
		return
	}
	*/

	if u := g.player.car; u != nil {
		// Check car-wall collision
		for _, c := range u.sprite.Draw() {
			x, y := c.XY()
			if row, ok := g.world[u.y + y]; ok {
				a, b := row.Wall()
				if u.x + x == a || u.x + x == b {
					g.Lose()
					return
				}
			}
		}

		// Check car-car collision
		for _, other := range g.unit {
			if other == u {
				continue
			}

			dx := other.x - u.x
			dy := other.y - u.y
			if dx < -ETC_CAR_SIZE || dx > ETC_CAR_SIZE ||
				dy < -ETC_CAR_SIZE || dy > ETC_CAR_SIZE {
				continue
			}

			for _, c := range u.sprite.Draw() {
				x, y := c.XY()
				ux, uy := u.x + x, u.y + y
				for _, c2 := range other.sprite.Draw() {
					x2, y2 := c2.XY()
					ox, oy := other.x + x2, other.y + y2
					if ux == ox && uy == oy {
						g.Lose()
						return
					}
				}
			}
		}
	}
}

func (g *Game) Score() float64 {
	return float64(g.top - GAME_HEIGHT)
}

func (g *Game) Command(e Event) {
	if l := len(g.dialogQ); l > g.dialog {
		if !g.life && g.dialog == l - 1 {
			return
		}
		if e == EventNextDialog {
			g.dialog++
		}
		return
	}

	if !g.life {
		return
	}

	switch e {
	case EventMoveLeft:
		g.player.Move(LEFT, 1)
	case EventMoveRight:
		g.player.Move(RIGHT, 1)
	case EventMoveLeftLane:
		n := 1
		if g.player.car != nil {
			n = world.CONFIG_LANE_WIDTH + 1
		}
		g.player.Move(LEFT, n)
	case EventMoveRightLane:
		n := 1
		if g.player.car != nil {
			n = world.CONFIG_LANE_WIDTH + 1
		}
		g.player.Move(RIGHT, n)
	case EventMoveUp:
		if g.player.car != nil {
			g.player.UpdateSpeed(
				g.player.speed + CONFIG_PLAYER_SPEED_CHANGE)
		} else {
			g.player.Move(UP, 1)
		}
	case EventMoveDown:
		if g.player.car != nil {
			g.player.UpdateSpeed(
				g.player.speed - CONFIG_PLAYER_SPEED_CHANGE)
		} else {
			g.player.Move(DOWN, 1)
		}
	case EventEngineStart:
		x, y := g.player.x, g.player.y
		for _, u := range g.unit {
			if u.x == x && u.y == y {
				g.player.car = u
				u.sprite.SetOption(STR_CAR_OPEN, STR_FALSE)
				u.sprite.SetOption(STR_CAR_PLAYER, STR_TRUE)
			}
		}
	case EventEngineStop:
		g.player.speed = 0
		if u := g.player.car; u != nil {
			u.sprite.SetOption(STR_CAR_OPEN, STR_TRUE)
			u.sprite.UnsetOption(STR_CAR_PLAYER)
		}
		g.player.car = nil
	}
}
