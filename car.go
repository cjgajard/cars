package main

type Car struct {
	OptionMap
	left *Unit
	right *Unit
}

func (c Car) Color() int {
	if v, ok := c.Option(STR_CAR_PLAYER); ok && v == STR_TRUE {
		return ETC_PLAYER_COLOR
	}
	return -2
}

func (c Car) Draw() []IComponent {
	l := []IComponent{
		newComponent('o', 0, 2),
		newComponent('-', 1, 2),
		newComponent('o', 2, 2),
		newComponent('0', -1, 1),
		newComponent('-', 0, 1),
		newComponent('-', 1, 1),
		newComponent('-', 2, 1),
		newComponent('0', 3, 1),
		// newComponent('|', -1, 0)),
		newComponent('|', 3, 0),
		newComponent('0', -1, -1),
		newComponent('_', 0, -1),
		newComponent('_', 1, -1),
		newComponent('_', 2, -1),
		newComponent('0', 3, -1),
	}

	if v, ok := c.Option(STR_CAR_OPEN); ok && v == STR_TRUE {
		l = append(l, newComponent('/', -2, 0))
	} else {
		l = append(l, newComponent('|', -1, 0))
	}

	if u := c.left; u != nil {
		l = append(l, u.sprite.Draw()[0])
	}

	if u := c.right; u != nil {
		l = append(l, u.sprite.Draw()[0])
	}

	return l
}

func NewCar(x, y int, l, r *Unit) *Unit {
	return &Unit{x: x, y: y, sprite: Car{
		left: l, right: r,
		OptionMap: *NewOptionMap(),
	}}
}
