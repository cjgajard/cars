package box

import (
	"regexp"
	"strings"
)

var spaceRegex = regexp.MustCompile(`\s+`)

const (
	BOX_BOTTOM_LEFT = '\u2514'
	BOX_BOTTOM_RIGHT = '\u2518'
	BOX_H = '\u2500'
	BOX_TOP_LEFT = '\u250c'
	BOX_TOP_RIGHT = '\u2510'
	BOX_V = '\u2502'
	BOX_EMPTY = ' '
	BOX_COLON = ':'
)

type Align uint8
const (
	AlignLeft Align = iota
	AlignCenter
	AlignRight
)

type row map[int]rune
type box map[int]row

type descriptor struct {
	box
	width int
}

func newDescriptor(w int) *descriptor {
	return &descriptor{box: make(box), width: w}
}

func (d *descriptor) Set(x, y int, r rune) {
	if _, ok := d.box[y]; ok {
		d.box[y][x] = r
		return
	}
	d.box[y] = make(row)
	d.Set(x, y, r)
}

func (d descriptor) Box() box {
	return d.box
}

func Print(w int, s [2]string, a Align) box {
	pad := 2
	d := newDescriptor(w)
	label, text := s[0], s[1]

	// Content
	y := 1
	word := spaceRegex.Split(text, -1)
	wl := len(word) // word length
	li := 0 // last word index
	mw := w - 2 * pad // max width

	for i, cw := 0, 0; i < wl; cw, y, li = 0, y + 1, i {
		for {
			if i < wl {
				cw += len(word[i])
			}
			if i > li {
				cw++
			}
			if cw > mw || i >= wl {
				break
			}
			i++
		}

		line := strings.Join(word[li:i], " ")
		ll := len(line) // line length

		// Get alignment
		var spacing int
		switch a {
		case AlignLeft:
			spacing = pad
		case AlignCenter:
			spacing = (w - ll) / 2
		case AlignRight:
			spacing = w - ll - pad
		}

		// Save line cells
		for x := 0; x < w; x++ {
			var r rune
			switch x {
			case 0, w - 1:
				r = BOX_V
			default:
				r = BOX_EMPTY
				if i := x - spacing; i >= 0 && i < ll {
					r = rune(line[i])
				}
			}
			d.Set(x, y, r)
		}
	}

	// Top and bottom borders
	for x := 0; x < w; x++ {
		var rb, rt rune

		if x == 0 {
			rt = BOX_TOP_LEFT
			rb = BOX_BOTTOM_LEFT
		} else if x == w - 1 {
			rt = BOX_TOP_RIGHT
			rb = BOX_BOTTOM_RIGHT
		} else {
			rt = BOX_H
			rb = BOX_H
			d.Set(x, y, BOX_H)
		}

		d.Set(x, 0, rt)
		d.Set(x, y, rb)
	}

	// Label
	if l := len(label); l > 0 {
		y := 0
		d.Set(pad, y, BOX_EMPTY)
		for x, r := range label {
			d.Set(x + pad + 1, y, r)
		}
		d.Set(l + pad + 1, y, BOX_COLON)
		d.Set(l + pad + 2, y, BOX_EMPTY)
	}

	return d.box
}
