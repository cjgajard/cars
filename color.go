package main

import (
	"github.com/nsf/termbox-go" // termbox
)

const (
	ColorAsphalt = termbox.Attribute(237)
	ColorDirt = termbox.Attribute(235)
)
