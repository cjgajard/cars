package world

import (
	"math/rand"
)

type TileType uint

type Tile struct {
	Type TileType
}

type Row struct {
	Size int
	Offset int
	Tile map[int]Tile
	Lane int
	TargetOffset int
}

func (r Row) Wall() (int, int) {
	return r.Offset, r.Offset + r.Size + r.Lane
}

type Game interface {
	World() map[int]Row
	Top() int
	Size() (int, int)
}

type Generator struct {
	height int
}

func New(h int) Generator {
	return Generator{height: h}
}

func (gen Generator) GenerateNextRow(w int, row Row) Row {
	ns := row.Size
	no := row.Offset
	nl := row.Lane
	nto := row.TargetOffset

	ok := ns == nl * CONFIG_LANE_WIDTH && nto == no
	if ok {
		if p := rand.Float64(); p < RNG_SIZE_CHANGE {
			max := (w - nto - nl - 1) / CONFIG_LANE_WIDTH;
			nl += randStep()
			nl = minmax(nl, CONFIG_LANE_MIN, max + 1)
		}
	}
	ns = step(ns, nl * CONFIG_LANE_WIDTH)
	if ok {
		if p := rand.Float64(); p < RNG_OFFSET_CHANGE {
			nto += randStep() * (CONFIG_LANE_WIDTH + 1)
			max := w - ns - nl
			nto = minmax(nto, 0, max)
		}
	}
	no = step(no, nto)

	nrow := Row{
		Size: ns,
		Offset: no,
		Lane: nl,
		TargetOffset: nto,
		Tile: make(map[int]Tile)}

	return nrow
}

func (gen Generator) Init(g Game) {
	gw, _ := g.Size()

	i := 0
	for ; i < 10; i++ {
		nl := CONFIG_LANE_INIT
		ns := nl * CONFIG_LANE_WIDTH
		no := (gw - ns) / 2
		nto := no
		nrow := Row{
			Size: ns,
			Offset: no,
			Lane: nl,
			TargetOffset: nto,
			Tile: make(map[int]Tile)}
		g.World()[i] = nrow
	}
	for ; i < gen.height; i++ {
		row := g.World()[i - 1]
		nrow := gen.GenerateNextRow(gw, row)
		g.World()[i] = nrow
	}
}

func (gen Generator) Height() int {
	return gen.height
}
