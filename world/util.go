package world

import (
	"math/rand"
)

func step(from, to int) int {
	if to > from {
		return from + 1
	} else if to < from {
		return from - 1
	}
	return from
}

func randStep() int {
	return rand.Intn(3) - 1
}

func minmax(n, a, b int) int {
	if n < a {
		return a
	} else if n < b {
		return n
	}
	return b - 1
}
