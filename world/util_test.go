package world

import (
	"testing"
)

func TestMinMax(t *testing.T) {
	if x := minmax(10, 0, 2); x != 1 {
		t.Fatalf("expected 1 but got %#v", x)
	}
	if x := minmax(0, 10, 100); x != 10 {
		t.Fatalf("expected 10 but got %#v", x)
	}
	if x := minmax(30, 10, 100); x != 30 {
		t.Fatalf("expected 30 but got %#v", x)
	}
}
