package main

type IComponent interface {
	Rune() rune
	XY() (int, int)
}

type component struct {
	r rune
	x int
	y int
}

func (c component) Rune() rune {
	return c.r
}

func (c component) XY() (int, int) {
	return c.x, c.y
}

func newComponent(r rune, x, y int) component {
	return component{r: r, x: x, y: y}
}

type Sprite interface {
	Color() int
	Draw() []IComponent
	Option(string) (string, bool)
	SetOption(string, string)
	UnsetOption(string)
}

type OptionMap struct {
	options map[string]string
}

func NewOptionMap() *OptionMap {
	return &OptionMap{options: make(map[string]string)}
}

func (o OptionMap) Option(k string) (string, bool) {
	v, ok := o.options[k]
	return v, ok
}

func (o OptionMap) SetOption(k string, v string) {
	o.options[k] = v
}

func (o OptionMap) UnsetOption(k string) {
	delete(o.options, k)
}

type Unit struct {
	x int
	y int
	sprite Sprite
}
