package main

type PlayerSprite struct {
	OptionMap
}

func (p PlayerSprite) Color() int {
	return ETC_PLAYER_COLOR
}

func (p PlayerSprite) Draw() []IComponent {
	return []IComponent{
		newComponent('@', 0, 0),
	}
}

type Player struct {
	Unit
	car *Unit
	speed int
	speedLimit int
}

func (p *Player) UpdateSpeed(value int) {
	if s := p.speedLimit; value > s {
		p.speed = s
	} else if s2 := -s / 2; value < s2 {
		p.speed = s2
	} else {
		p.speed = value
	}
}

func (p *Player) Move(d Dir, n int) {
	var dx int
	var dy int

	switch d {
	case UP:
		dx = 0
		dy = n
	case RIGHT:
		dx = n
		dy = 0
	case DOWN:
		dx = 0
		dy = -n
	case LEFT:
		dx = -n
		dy = 0
	}

	p.x += dx
	p.y += dy
	if p.car != nil {
		p.car.x += dx
		p.car.y += dy
	}
}

func NewPlayer() *Player {
	return &Player{
		Unit: Unit{
			x: GAME_WIDTH / 2,
			y: 1,
			sprite: PlayerSprite{
				OptionMap: *NewOptionMap(),
			},
		},
		speed: 0,
		speedLimit: CONFIG_PLAYER_SPEED_LIMIT,
	}
}
