package main

import (
	"github.com/nsf/termbox-go"
)

type Event uint
const (
	EventNone = iota
	EventQuit
	EventMoveLeft
	EventMoveRight
	EventMoveRightLane
	EventMoveLeftLane
	EventMoveUp
	EventMoveDown
	EventLose
	EventEngineStart
	EventEngineStop
	EventNextDialog
)

type TermboxEventor struct {
}

func (e *TermboxEventor) Event(ch chan Event) {
	switch ev := termbox.PollEvent(); ev.Type {
	case termbox.EventKey:
		if ev.Key == termbox.KeyEsc {
			ch <- EventQuit
		}
		switch ev.Ch {
		case 'h':
			ch <- EventMoveLeft
		case 'l':
			ch <- EventMoveRight
		case 'H':
			ch <- EventMoveLeftLane
		case 'L':
			ch <- EventMoveRightLane
		case 'k':
			ch <- EventMoveUp
		case 'j':
			ch <- EventMoveDown
		case 'i':
			ch <- EventEngineStart
		case 'u':
			ch <- EventEngineStop
		case 'n':
			ch <- EventNextDialog
		}
	}
}
