package main

import (
	"time"
	"math/rand"
	"gitlab.com/cjgajard/cars/world"
)

var wgen = world.New(200)

func main() {
	rand.Seed(time.Now().Unix())
	g := NewGame()
	e := &TermboxEventor{}
	p := &TermboxPrinter{game: g}
	ch := make(chan Event)

	defer p.Close()
	wgen.Init(g)
	g.Init()
	p.Init()

	go func() {
		for {
			e.Event(ch)
		}
	}()

	go func() {
		for {
			g.UpdateCamera(ch)
		}
	}()

	for {
		select {
		case ev := <-ch:
			switch ev {
			case EventQuit:
				return
			default:
				g.Command(ev)
			}
		default:
			g.Update()
			p.Draw()
		}
	}
}
